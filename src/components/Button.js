function Button({ setSales, products }) {
  const generateSale = () => {
    const randomProductId = Math.floor(Math.random() * 6) + 1;
    const randomDiscount = Math.floor(Math.random() * 50) + 40; // 40% e 90%

    const discountedItem = products.filter(
      (prod) => prod.id === randomProductId
    )[0];

    const rawDiscount = (randomDiscount / 100) * discountedItem.price;

    discountedItem.discount = Math.round(rawDiscount);
    discountedItem.newPrice = Math.round(discountedItem.price - rawDiscount);
    discountedItem.sale = randomDiscount;

    setSales((prev) => [...prev, discountedItem]);
  };

  return <button onClick={generateSale}>GERAR PROMOÇÃO</button>;
}

export default Button;
