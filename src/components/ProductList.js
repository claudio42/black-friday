import Product from "./Product";
function ProductList({ productsGenerated, handleClick }) {
  return (
    <div>
      {productsGenerated.map((prod, idx) => {
        return (
          <Product
            key={idx}
            productData={prod}
            handleClick={handleClick && (() => handleClick(prod.id))}
          />
        );
      })}
    </div>
  );
}

export default ProductList;
