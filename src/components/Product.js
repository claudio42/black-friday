function Product({ productData, handleClick }) {
  return (
    <div>
      <h3>{productData.name}</h3>
      <p>Preço original: {productData.price}$</p>
      <p>Porcentagem de desconto:{productData.sale}%</p>
      <p>Valor do desconto: {productData.discount}$</p>
      <p>Preço a ser pago: {productData.newPrice}$</p>
      {handleClick && (
        <button onClick={handleClick}>Adicionar ao carrinho</button>
      )}
    </div>
  );
}

export default Product;
