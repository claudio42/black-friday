import React, { useState } from "react";
import Button from "./components/Button";

import ProductList from "./components/ProductList";

function App() {
  const [sales, setSales] = useState([]);
  const [currentSale, setCurrentSale] = useState([]);

  const products = [
    { id: 1, name: "Smart TV LED 50", price: 1999.0 },
    { id: 2, name: "Playstation 5", price: 12000.0 },
    { id: 3, name: "Notebook Acer Nitro 5", price: 5109.72 },
    { id: 4, name: "Headset s fio Logitech G935", price: 1359.0 },
    { id: 5, name: "Tablet Samsung Galaxy Tab S7", price: 4844.05 },
    { id: 6, name: "Cadeira Gamer Cruiser Preta FORTREK", price: 1215.16 },
  ];

  const handleClick = (productId) => {
    const newItem = sales.find((item) => item.id === productId);
    setCurrentSale([...currentSale, newItem]);
  };

  const cartTotal = currentSale.reduce((total, element) => {
    return (total += element.newPrice);
  }, 0);

  return (
    <div>
      <h1>Loja</h1>
      <Button setSales={setSales} products={products} />
      <ProductList productsGenerated={sales} handleClick={handleClick} />
      <h2>Carrinho</h2>
      <ProductList productsGenerated={currentSale} handleClick={false} />
      <h2>Total</h2>
      {cartTotal}
    </div>
  );
}

export default App;
